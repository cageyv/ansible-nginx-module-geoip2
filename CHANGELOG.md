# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.1](https://gitlab.com/guardianproject-ops/ansible-nginx-module-geoip2/compare/0.1.0...0.1.1) (2020-07-02)


### Bug Fixes

* Rename internal var to not clobber the global namespace ([ae1f6de](https://gitlab.com/guardianproject-ops/ansible-nginx-module-geoip2/commit/ae1f6de7a6c34b398cdbebf8b8bcf78f81f17ae8))

## [0.1.0] - 2020-04-10

### Added

- This CHANGELOG
- Initial version

### Changed

n/a

### Removed

n/a

[Unreleased]: https://gitlab.com/guardianproject-ops/ansible-nginx-module-geoip2/compare/0.1.0...master
[0.1.0]: https://gitlab.com/guardianproject-ops/ansible-nginx-module-geoip2/-/tags/0.1.0
